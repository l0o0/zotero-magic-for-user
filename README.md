![背景](pngs/%E8%83%8C%E6%99%AF.png)

## 主要功能
- 全文翻译（基于翻译插件翻译引擎，不限次数永久使用，具有很强的交互，轻松看到原文）
- 文献总结（基于GPT，永久使用，可以看到总结过程，自定义总结框架/角度/内容）

## TODO
- [x] 参考文献可选翻译
- [x] 优化pdf关闭后暂停翻译
- [x] Style的全文翻译，写入笔记形式
- [x] 基于GPT & 笔记的论文概括自动生成

## 更新日志

### 1.2.9
- 导出翻译后的PDF全文适配Mac
- 优化解析失败bug

Mac字体路径参考 `/Users/Polygon/Library/Fonts/Kaiu.ttf`


### 1.2.8
- 支持输出File，导出翻译后的PDF全文（仅仅适配Windows）

Mac路径参考 `/Users/Polygon/Library/Fonts/Kaiu.ttf`

![输入图片说明](pngs/pdf%E5%AF%BC%E5%87%BA.png)

![输入图片说明](pngs/%E7%BF%BB%E8%AF%91%E5%90%8Epdf.png)

### 1.2.7
- 优化用户体验

### 1.2.6
- 支持双屏关联横向/竖向分割设定
- 支持翻译主/次窗口设定

![输入图片说明](pngs/1.2.6%E9%A2%84%E8%A7%88.png)

### 1.2.5
- 支持多文献批量生成文献总结

<details>

<summary>使用步骤</summary>

1. 首选项，更新paper summary模板
2. 更新最新版magic插件(>=1.2.5)
3. 添加[Actions & Tags](https://github.com/windingwind/zotero-actions-tags/releases)命令

代码如下

```js
const Zotero = require("Zotero")
const ZoteroPane = require("ZoteroPane")

for (let item of items) {
	const noteItem = new Zotero.Item("note")
	noteItem.parentID = item.id;
	noteItem.setNote("正在生成中...")
	await noteItem.saveTx();
	ZoteroPane.selectItem(noteItem.id)
	// "[text] Paper Summary" 可修改为你想批量运行的模板名称，注意：一字不能差！
	const html = await Zotero.BetterNotes.api.template.runTextTemplate("[text] Paper Summary", {
		targetNoteId: noteItem.id
	})
	noteItem.setNote(html)
	await noteItem.saveTx();
}

```

配置如图

![AT配置](pngs/AT-paper-summary.png)

4. 在Zotero主界面，多选文献，右键

![输入图片说明](%E5%8F%B3%E9%94%AE%E6%89%B9%E9%87%8F%E6%8F%92%E5%85%A5%E7%AC%94%E8%AE%B0%E6%A8%A1%E6%9D%BF.png)


5. 去做其他的事情，让它慢慢生成

![输入图片说明](pngs/%E4%B8%BB%E7%95%8C%E9%9D%A2GPT%E7%94%9F%E6%88%90%E6%BC%94%E7%A4%BA.png)

</details>

### 1.2.4
- 支持标定正文区域

![输入图片说明](pngs/%E6%AD%A3%E6%96%87%E5%8C%BA%E5%9F%9F%E6%A0%87%E6%B3%A8.png)

### 1.2.3
- 优化文献解析能力
- 修复新增tab自动翻译bug

### 1.2.2
- 优化文献解析能力

### 1.2.1
- 优化文献解析能力
- 优化全文翻译解析

### 1.2.0
- 新增支持**文献总结**
<details>
<summary>如何使用?</summary>

0. 确保你是`Zotero 7`
1. 升级Magic插件到1.2.0或以上
2. 无论你有没有GPT插件，下载本仓库的GPT插件，[点击此处](zotero-gpt.xpi)，若你初次使用GPT插件，需要配置密钥，[参考视频](https://www.bilibili.com/video/BV17N4y1o7vx)
3. 安装Better Notes插件（已经安装的忽略此步骤），[点击此处](https://github.com/windingwind/zotero-better-notes/releases/download/1.1.4-24/zotero-better-notes.xpi)
4. [点击此处](paper-summary.yaml)获取笔记模板，点击一键复制，复制这个Better Notes的笔记模板

![笔记模板复制](pngs/%E7%AC%94%E8%AE%B0%E6%A8%A1%E6%9D%BF%E5%A4%8D%E5%88%B6.png)

5. 打开Zotero，根据下图，导入模板

![笔记模板导入](pngs/%E4%BB%8E%E5%89%AA%E8%B4%B4%E6%9D%BF%E5%AF%BC%E5%85%A5%E7%AC%94%E8%AE%B0%E6%A8%A1%E6%9D%BF.png)

6. 在Zotero中导入[示例文献:http://doi.org/10.7717/peerj.10542](https://peerj.com/articles/10542/)，并打开，根据下图插入模板


![模板插入](pngs/%E6%89%93%E5%BC%80pdf%E6%8F%92%E5%85%A5%E7%AC%94%E8%AE%B0%E6%A8%A1%E6%9D%BF.png)

7. 点击`Paper Summary`

![模板选择](pngs/%E6%A8%A1%E6%9D%BF%E9%80%89%E6%8B%A9.png)

8. 等待 GPT 总结，期间不要有任何操作，GPT 插件会全自动问答

![输入图片说明](pngs/GPT%E6%80%BB%E7%BB%93.png)


9. 查看，你可以直接点开笔记查看，也可以配合Zotero-Style插件在主界面查看

![笔记查看](pngs/GPT%E7%AC%94%E8%AE%B0%E6%9F%A5%E7%9C%8B.png)

![笔记查看](pngs/%E4%B8%BB%E7%95%8C%E9%9D%A2GPT%E7%AC%94%E8%AE%B0%E6%9F%A5%E7%9C%8B.png)
</details>


### 1.1.7
- 提供设置
- 优化翻译

![输入图片说明](pngs/image.png)


### 1.1.6
- 修复参考文献截断过早bug
- 翻译结果缓存单pdf单json，防止卡顿
- 优化全文翻译/显示原文状态显示
- 优化段落匹配

### 1.1.5
- 显示邀请状态

### 1.1.4
- 优化卡顿
- 优化参考文献截断词
- 取消悬浮原文显示，请在翻译插件侧边栏查看

### 1.1.3
- 重写渲染逻辑
- 翻译侧边栏源文本/翻译文本跟随鼠标悬浮span
- 优化关闭PDF停止翻译

![侧边栏](pngs/%E4%BE%A7%E8%BE%B9%E6%A0%8F%E6%96%87%E6%9C%AC.png)

### 1.1.2
- 优化翻译至笔记成功率
- 优化文档解析

### 1.1.1
- 优化翻译至笔记提示
- 优化储存缓存，修复缓存失败

### 1.1.0
- 实现翻译至笔记选项

### 1.0.9
- Ctrl+单击不复制
- 修复激活失败

### 1.0.8
- 逐句/逐段翻译
- 优化分段
- 单击翻译结果复制原文&译文

### 1.0.7
- `全文翻译/显示原文`切换至原文
- 优化分段（tail indent分段，distance分段）
- 优化翻译速度

### 1.0.6
- [鼠标hover移除提示文字，仅保留翻译引擎](https://gitee.com/MuiseDestiny/zotero-magic-for-user/issues/I7XX6S)
- [修复页面卡死](https://gitee.com/MuiseDestiny/zotero-magic-for-user/issues/I7XX9K)
- 移除页面段落阻塞，翻译更快！


### 1.0.5
本次更新需要满足翻译插件版本 >= [1.1.0-14](https://gitee.com/MuiseDestiny/zotero-magic-for-user/raw/master/zotero-pdf-translate-1.1.0-14.xpi)
- 修复段落解析失败
- 翻译引擎可在Magic内部单独指定与翻译插件独立
- 优化多翻译结果叠加bug
- 移除首次打开Zotero欢迎弹窗
- 优化重复翻译体验
- 优化段落解析

### 1.0.4
- 遇到参考文献截断词停止翻译
- 保存翻译结果

### 1.0.3
- 修复行距过大导致段落识别失败
- 修复其它语言翻译为中文错误（负载均衡开启时）
- 修复三栏或多栏PDF翻译失败
- 优化翻译卡住问题
- 优化双屏互联体验
